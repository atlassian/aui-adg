AUI ADG
=======

As of AUI 7, the AUI ADG assets reside in the [main AUI repository](https://bitbucket.org/atlassian/aui).

This repository exists to support older 6.x releases of AUI.

Check out a branch to work with this repository.